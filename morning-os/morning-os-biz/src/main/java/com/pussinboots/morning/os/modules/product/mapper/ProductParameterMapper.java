package com.pussinboots.morning.os.modules.product.mapper;

import com.pussinboots.morning.os.modules.product.entity.ProductParameter;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * 
* 项目名称：morning-os-biz   
* 类名称：ProductParameterMapper   
* 类描述：ProductParameter表 / 商品参数表 数据访问层  
* 创建人：陈星星   
* 创建时间：2017年3月4日 上午4:28:50   
*
 */
public interface ProductParameterMapper extends BaseMapper<ProductParameter> {

}