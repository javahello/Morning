package com.pussinboots.morning.os.modules.product.mapper;

import com.pussinboots.morning.os.modules.product.entity.Label;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * 
* 项目名称：morning-os-biz   
* 类名称：LabelMapper   
* 类描述：Label表 / 商品标签表  数据访问层接口   
* 创建人：陈星星   
* 创建时间：2017年2月23日 下午3:45:43   
*
 */
public interface LabelMapper extends BaseMapper<Label> {

}