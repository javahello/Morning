package com.pussinboots.morning.os.modules.product.service.impl;

import com.pussinboots.morning.os.modules.product.entity.KindAttribute;
import com.pussinboots.morning.os.modules.product.mapper.KindAttributeMapper;
import com.pussinboots.morning.os.modules.product.service.IKindAttributeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 
* 项目名称：morning-os-biz   
* 类名称：KindAttributeServiceImpl   
* 类描述：KindAttribute 表   / 类型属性表 业务逻辑层接口实现 
* 创建人：陈星星   
* 创建时间：2017年3月5日 下午5:40:03   
*
 */
@Service
public class KindAttributeServiceImpl extends ServiceImpl<KindAttributeMapper, KindAttribute> implements IKindAttributeService {
	
}
