package com.pussinboots.morning.os.modules.user.mapper;

import com.pussinboots.morning.os.modules.user.entity.Favorite;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * 
* 项目名称：morning-os-biz   
* 类名称：FavoriteMapper   
* 类描述：Favorite表 / 收藏夹表 数据访问层接口   
* 创建人：陈星星   
* 创建时间：2017年3月15日 下午11:09:41   
*
 */
public interface FavoriteMapper extends BaseMapper<Favorite> {
	
}