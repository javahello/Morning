package com.pussinboots.morning.os.modules.product.mapper;

import com.pussinboots.morning.os.modules.product.entity.ProductAttribute;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * 
* 项目名称：morning-os-biz   
* 类名称：ProductAttributeMapper   
* 类描述：ProductAttribute表 / 商品属性表 数据访问层接口   
* 创建人：陈星星   
* 创建时间：2017年2月27日 下午1:29:02   
*
 */
public interface ProductAttributeMapper extends BaseMapper<ProductAttribute> {

}