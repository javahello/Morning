package com.pussinboots.morning.os.modules.user.mapper;

import com.pussinboots.morning.os.modules.user.entity.Address;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * 
* 项目名称：morning-os-biz   
* 类名称：AddressMapper   
* 类描述：Address表 / 收获地址表数据访问层接口
* 创建人：陈星星   
* 创建时间：2017年3月16日 下午6:58:23   
*
 */
public interface AddressMapper extends BaseMapper<Address> {

}