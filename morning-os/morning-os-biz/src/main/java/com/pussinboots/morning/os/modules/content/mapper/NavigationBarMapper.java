package com.pussinboots.morning.os.modules.content.mapper;

import com.pussinboots.morning.os.modules.content.entity.NavigationBar;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * 
* 项目名称：morning-os-biz   
* 类名称：NavigationBarMapper   
* 类描述：NavigationBar表 / 导航栏表 数据访问层接口   
* 创建人：陈星星   
* 创建时间：2017年2月23日 下午11:19:29   
*
 */
public interface NavigationBarMapper extends BaseMapper<NavigationBar> {

}