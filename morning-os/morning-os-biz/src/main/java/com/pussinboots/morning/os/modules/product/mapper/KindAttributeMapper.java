package com.pussinboots.morning.os.modules.product.mapper;

import com.pussinboots.morning.os.modules.product.entity.KindAttribute;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * 
* 项目名称：morning-os-biz   
* 类名称：KindAttributeMapper   
* 类描述： KindAttribute表 / 类型属性表 数据访问层接口  
* 创建人：陈星星   
* 创建时间：2017年3月5日 下午5:36:52   
*
 */
public interface KindAttributeMapper extends BaseMapper<KindAttribute> {

}