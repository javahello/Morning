package com.pussinboots.morning.os.modules.product.service.impl;

import com.pussinboots.morning.os.modules.product.entity.SpecificationAttribute;
import com.pussinboots.morning.os.modules.product.mapper.SpecificationAttributeMapper;
import com.pussinboots.morning.os.modules.product.service.ISpecificationAttributeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 
* 项目名称：morning-os-biz   
* 类名称：SpecificationAttributeServiceImpl   
* 类描述：SpecificationAttribute 表   / 规格属性表 业务逻辑层接口实现     
* 创建人：陈星星   
* 创建时间：2017年3月5日 下午5:40:49   
*
 */
@Service
public class SpecificationAttributeServiceImpl extends ServiceImpl<SpecificationAttributeMapper, SpecificationAttribute> implements ISpecificationAttributeService {
	
}
