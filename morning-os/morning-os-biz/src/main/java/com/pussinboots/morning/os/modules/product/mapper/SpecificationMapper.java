package com.pussinboots.morning.os.modules.product.mapper;

import com.pussinboots.morning.os.modules.product.entity.Specification;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * 
* 项目名称：morning-os-biz   
* 类名称：SpecificationMapper   
* 类描述：Specification表 / 规格表 数据访问层接口  
* 创建人：陈星星   
* 创建时间：2017年3月5日 下午5:39:23   
*
 */
public interface SpecificationMapper extends BaseMapper<Specification> {

}