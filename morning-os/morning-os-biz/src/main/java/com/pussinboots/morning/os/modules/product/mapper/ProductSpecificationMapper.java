package com.pussinboots.morning.os.modules.product.mapper;

import com.pussinboots.morning.os.modules.product.entity.ProductSpecification;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * 
* 项目名称：morning-os-biz   
* 类名称：ProductSpecificationMapper   
* 类描述：ProductSpecification 表 / 商品规格表 数据访问层接口  
* 创建人：陈星星   
* 创建时间：2017年3月5日 下午9:57:46   
*
 */
public interface ProductSpecificationMapper extends BaseMapper<ProductSpecification> {

}