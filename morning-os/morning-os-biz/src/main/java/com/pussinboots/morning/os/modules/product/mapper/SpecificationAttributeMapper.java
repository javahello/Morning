package com.pussinboots.morning.os.modules.product.mapper;

import com.pussinboots.morning.os.modules.product.entity.SpecificationAttribute;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * 
* 项目名称：morning-os-biz   
* 类名称：SpecificationAttributeMapper   
* 类描述： SpecificationAttribute表 / 规格属性表 数据访问层接口  
* 创建人：陈星星   
* 创建时间：2017年3月5日 下午5:36:52   
*
 */
public interface SpecificationAttributeMapper extends BaseMapper<SpecificationAttribute> {
	
	/**
	 * 根据规格属性ID列表查找规格属性名称列表
	 * @param spec 规格属性ID列表
	 * @return List<String> 
	 */
	List<String> selectBySpec(String spec);

}