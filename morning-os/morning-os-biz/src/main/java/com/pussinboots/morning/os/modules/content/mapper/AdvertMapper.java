package com.pussinboots.morning.os.modules.content.mapper;

import com.pussinboots.morning.os.modules.content.entity.Advert;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * 
* 项目名称：morning-os-biz   
* 类名称：AdvertMapper   
* 类描述：Advert表 / 广告位表 数据访问层接口   
* 创建人：陈星星   
* 创建时间：2017年2月23日 下午11:19:29   
*
 */
public interface AdvertMapper extends BaseMapper<Advert> {

}