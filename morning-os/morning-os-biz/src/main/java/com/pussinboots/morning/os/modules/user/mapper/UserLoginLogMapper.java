package com.pussinboots.morning.os.modules.user.mapper;

import com.pussinboots.morning.os.modules.user.entity.UserLoginLog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * 
* 项目名称：morning-os-biz   
* 类名称：UserLoginLogMapper   
* 类描述：UserLoginLog 表数据访问层接口           
* 创建人：陈星星   
* 创建时间：2017年2月20日 上午1:13:09    
*
 */
public interface UserLoginLogMapper extends BaseMapper<UserLoginLog> {

}