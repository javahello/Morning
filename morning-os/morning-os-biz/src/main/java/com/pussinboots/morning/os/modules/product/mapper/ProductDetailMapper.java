package com.pussinboots.morning.os.modules.product.mapper;

import com.pussinboots.morning.os.modules.product.entity.ProductDetail;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * 
* 项目名称：morning-os-biz   
* 类名称：ProductDetailMapper   
* 类描述：ProductDetail表 / 商品描述表 数据访问层接口   
* 创建人：陈星星   
* 创建时间：2017年2月23日 下午3:46:38   
*
 */
public interface ProductDetailMapper extends BaseMapper<ProductDetail> {

}