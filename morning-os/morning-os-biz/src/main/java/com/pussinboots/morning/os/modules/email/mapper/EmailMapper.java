package com.pussinboots.morning.os.modules.email.mapper;

import com.pussinboots.morning.os.modules.email.entity.Email;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * 
* 项目名称：morning-os-biz   
* 类名称：EmailMapper   
* 类描述：Email表 数据访问层接口        
* 创建人：陈星星   
* 创建时间：2017年2月21日 下午10:56:40   
*
 */
public interface EmailMapper extends BaseMapper<Email> {

}