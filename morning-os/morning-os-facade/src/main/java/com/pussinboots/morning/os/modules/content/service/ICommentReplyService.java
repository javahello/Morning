package com.pussinboots.morning.os.modules.content.service;

import com.pussinboots.morning.os.modules.content.entity.CommentReply;
import com.baomidou.mybatisplus.service.IService;

/**
 * 
* 项目名称：morning-os-facade   
* 类名称：ICommentReplyService   
* 类描述： CommentReply表 / 评论回复表 业务逻辑层接口     
* 创建人：陈星星   
* 创建时间：2017年3月4日 下午3:11:51   
*
 */
public interface ICommentReplyService extends IService<CommentReply> {
	
}
