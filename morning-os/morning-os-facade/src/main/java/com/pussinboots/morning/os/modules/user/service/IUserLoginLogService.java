package com.pussinboots.morning.os.modules.user.service;

import com.pussinboots.morning.os.modules.user.entity.UserLoginLog;
import com.baomidou.mybatisplus.service.IService;

/**
 * 
* 项目名称：morning-os-facade   
* 类名称：IUserLoginLogService   
* 类描述：UserLoginLog 表业务逻辑层接口   
* 创建人：陈星星   
* 创建时间：2017年2月20日 上午1:10:41    
*
 */
public interface IUserLoginLogService extends IService<UserLoginLog> {
	
}
