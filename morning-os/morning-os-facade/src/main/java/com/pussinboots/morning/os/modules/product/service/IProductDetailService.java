package com.pussinboots.morning.os.modules.product.service;

import com.pussinboots.morning.os.modules.product.entity.ProductDetail;
import com.baomidou.mybatisplus.service.IService;

/**
 * 
* 项目名称：morning-os-facade   
* 类名称：IProductDetailService   
* 类描述：ProductDetail表 / 商品描述表 业务逻辑层接口  
* 创建人：陈星星   
* 创建时间：2017年2月23日 下午3:42:38   
*
 */
public interface IProductDetailService extends IService<ProductDetail> {
	
}
