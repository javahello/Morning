package com.pussinboots.morning.os.modules.product.service;

import com.pussinboots.morning.os.modules.product.entity.Label;
import com.baomidou.mybatisplus.service.IService;

/**
 * 
* 项目名称：morning-os-facade   
* 类名称：ILabelService   
* 类描述：Label表 / 商品标签表 业务逻辑层接口      
* 创建人：陈星星   
* 创建时间：2017年2月23日 下午3:41:49   
*
 */
public interface ILabelService extends IService<Label> {
	
}
