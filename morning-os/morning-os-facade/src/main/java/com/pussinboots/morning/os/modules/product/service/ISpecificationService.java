package com.pussinboots.morning.os.modules.product.service;

import com.pussinboots.morning.os.modules.product.entity.Specification;
import com.baomidou.mybatisplus.service.IService;

/**
 * 
* 项目名称：morning-os-facade   
* 类名称：ISpecificationService   
* 类描述：Specification表 / 规格表 业务逻辑层接口   
* 创建人：陈星星   
* 创建时间：2017年3月5日 下午5:35:15   
*
 */
public interface ISpecificationService extends IService<Specification> {
	
}
