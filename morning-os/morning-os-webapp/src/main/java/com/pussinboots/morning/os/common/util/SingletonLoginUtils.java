package com.pussinboots.morning.os.common.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import com.google.code.kaptcha.Constants;
import com.pussinboots.morning.common.util.ServletUtils;
import com.pussinboots.morning.os.common.security.AuthorizingUser;

/**
 * 
* 项目名称：morning-cms-webapp   
* 类名称：SingletonLoginUtils   
* 类描述：登录用户通用处理工具类   
* 创建人：陈星星   
* 创建时间：2017年2月3日 下午1:04:53
 */
public class SingletonLoginUtils {
	
	private SingletonLoginUtils() { }

	/**
	 * 验证验证码
	 * @param userInputCaptcha
	 * @return 正确:true/错误:false
	 */
	public static boolean validate() {
		// 获取Session中验证码
		Object captcha = ServletUtils.getAttribute(Constants.KAPTCHA_SESSION_KEY);
		String registerCode = ServletUtils.getParameter("registerCode");
		if (StringUtils.isEmpty(registerCode)) {
			return false;
		}
		boolean result = registerCode.equalsIgnoreCase(captcha.toString());
		if (result) {
			ServletUtils.getRequest().getSession().removeAttribute(Constants.KAPTCHA_SESSION_KEY);
		}
		return result;
	}
	
	/**
	 * 获取登录用户
	 * @return
	 */
	public static AuthorizingUser getUser() {
		Subject subject = SecurityUtils.getSubject();
		AuthorizingUser user = (AuthorizingUser) subject.getPrincipal();
		if (user != null) {
			return user;
		}
		return null;
	}
	
	/**
	 * 获取登录用户ID
	 * @return
	 */
	public static Long getUserId(){
		return getUser().getUserId();
	}
	
	/**
	 * 获取登录用户昵称
	 * @return
	 */
	public static String getUserName(){
		return getUser().getUserName();
	}
	
}
