package com.pussinboots.morning.os.modules.user.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.pussinboots.morning.common.controller.BaseController;

/**
 * 
* 项目名称：morning-os-webapp   
* 类名称：UserAccountController   
* 类描述：后台中心-订单中心表示层控制器   
* 创建人：陈星星   
* 创建时间：2017年3月14日 下午11:15:07   
*
 */
@Controller
@RequestMapping(value = "/uc/order")
public class UserOrderController extends BaseController {
	
}
