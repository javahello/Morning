package com.pussinboots.morning.cms.modules.administrator.mapper;

import com.pussinboots.morning.cms.modules.administrator.entity.Organization;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * 
* 项目名称：morning-cms-biz   
* 类名称：OrganizationMapper   
* 类描述：Organization 表数据访问层接口      
* 创建人：陈星星   
* 创建时间：2017年2月17日 上午12:09:39
 */
public interface OrganizationMapper extends BaseMapper<Organization> {

}