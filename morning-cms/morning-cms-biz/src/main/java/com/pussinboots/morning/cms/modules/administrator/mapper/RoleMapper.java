package com.pussinboots.morning.cms.modules.administrator.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.pussinboots.morning.cms.modules.administrator.entity.Role;

/**
 * 
* 项目名称：morning-cms-biz   
* 类名称：RoleMapper   
* 类描述：Role 表数据访问层接口   
* 创建人：陈星星   
* 创建时间：2017年2月3日 下午12:42:18
 */
public interface RoleMapper extends BaseMapper<Role> {

}