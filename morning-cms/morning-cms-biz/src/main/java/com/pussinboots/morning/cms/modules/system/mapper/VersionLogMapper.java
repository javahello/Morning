package com.pussinboots.morning.cms.modules.system.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.pussinboots.morning.cms.modules.system.entity.VersionLog;

/**
 * 
* 项目名称：morning-cms-biz   
* 类名称：VersionLogMapper   
* 类描述：VersionLog 表数据访问层接口      
* 创建人：陈星星   
* 创建时间：2017年2月8日 下午6:09:10
 */
public interface VersionLogMapper extends BaseMapper<VersionLog> {

}